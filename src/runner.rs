use std::sync::mpsc;
use std::{thread, time};

extern crate rand;
use self::rand::distributions::Distribution;
use self::rand::distributions::{Uniform};

pub fn run() {
    let mut all = vec![];

    for i in 0..10 {
        all.push(thread::spawn(move || {
            println!("T {}", i);
        }));
    }

    for t in all {
        let _ = t.join();
    }
}

pub fn run_in_cs() {
    let (tx, rv) = mpsc::channel::<i32>();
    let u = Uniform::from(500..1000);
    let mut rng = rand::thread_rng();

    for i in 0..10 {
        let t = tx.clone();
        let sleep = u.sample(&mut rng);

        thread::spawn(move || {
            thread::sleep(time::Duration::from_millis(sleep));
            let _ = t.send(i);
        });
    }

    let mut res = vec![];

    for _ in 0..10 {
        res.push(rv.recv().unwrap());
    }

    dbg!(res);
}
