use std::fmt;
mod runner;

use runner::*;

macro_rules! s {
    ($x:expr) => {
        $x.to_string()
    };
}

#[derive(Debug)]
struct C {
    x: String,
    y: i32,
}

struct Y(i32, i32);

impl fmt::Display for C {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Just: {}", self.x)
    }
}

impl fmt::Display for Y {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "First: {}, Second: {}", self.0, self.1)
    }
}

fn test(s: String) {
    let c = C {
        x: s! {"Yep"},
        y: 12,
    };

    println!("{:#?}", c);
    println!("{}", c);

    println!("{}", Y(1, 2));

    let mut v = vec![1, 2, 3, 4];

    println! {"{:?}",v}
    println! {"{:?}",v[3]}

    v.push(2);
    println! {"{:?}",v.len()}

    println! {"{}", s}
}

#[derive(Debug)]
struct Check {
    x: u32,
}

fn move_it() {
    let mut c = Check { x: 12 };
    let z = c;

    c.x = 11;
    println!("{:}", z.x);
}

fn print_it(s: String, c: Check) {
    println!("{} {:?}", s, c);
}

fn move_it1() {
    let s = String::from("moving");
    let c = dbg! {Check{x:1}};

    print_it(s, c);

    //println!("{}", s);
    //println!("{:?}", c);
}

fn get() -> &'static i32 {
    static V: [i32; 3] = [1, 2, 3];
    &V[0]
}

fn main() {
    let s1 = String::from("test");
    let s2 = s1;

    test(s2);
    move_it();

    move_it1();

    let i = dbg! {get()};
    println!("{}", i);

    run();
    run_in_cs();
}
